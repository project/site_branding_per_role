INTRODUCTION
------------
A simple module that provide functionality to add different
 path to brand according to roles.On click of brand user will redirect
 to particular path as per role.Admin can configure visibilty of site name,
 site logo and site slogan.

* For a full description of the module visit:
  https://www.drupal.org/project/site_branding_per_role


REQUIREMENTS
------------

NO special requirements.

INSTALLATION
------------

* Install the Site Branding Per Role module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.


CONFIGURATION
------------
1. After successfully installing the module Site Branding Per Role,
  you can place 'site branding per role block' on region. 
  admin -> structure -> block -> Place "Site branding per role block" on region.
2. Add a valid url to each roles

MAINTAINERS
-----------

Current maintainers:
* Sumit kumar (https://www.drupal.org/u/bsumit5577)
